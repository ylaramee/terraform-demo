

provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region  = "${var.aws_region}"
}

resource "aws_default_vpc" "default" {}

resource "aws_security_group" "default_security_group_ipv4" {
  vpc_id = "${aws_default_vpc.default.id}"
  name   = "Ensure_SSH_Allow_All_IPV4"

  ingress {
    to_port     = 22
    from_port   = 22
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_key_pair" "authorized_key" {
  key_name   = "terraform-demo-prez"
  public_key = "${var.authorized_key}"
}

/*data "aws_ami" "ubuntu_xenial" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
    ubuntu/images/hvm-ssd/ubuntu-trusty-14.04-amd64-server-*
  }

  owners      = ["099720109477"]
}*/

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"]
}




resource "aws_instance" "web" {
  count         = "${var.instances_count}"
  ami           = "${data.aws_ami.ubuntu.id}"
  instance_type = "${var.instances_type}"
  key_name      = "${aws_key_pair.authorized_key.key_name}"
  tags          = "${var.tags}"
}

data "aws_security_group" "allow_http" {
  name = "allow_http"
}

resource "aws_elb" "lb" {
  name               = "terraform-demo-elb-prez"
  instances          = ["${aws_instance.web.*.id}"]
  availability_zones = ["${aws_instance.web.*.availability_zone}"]
  security_groups    = ["${aws_default_vpc.default.default_security_group_id}", "${data.aws_security_group.allow_http.id}"]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 30
  }

  tags = "${var.tags}"
}

resource "ansible_host" "default" {
  count = "${aws_instance.web.count}"
  inventory_hostname = "${aws_instance.web.*.id[count.index]}"
  vars {
    ansible_user = "ubuntu"
    ansible_host = "${aws_instance.web.*.public_ip[count.index]}"
  }
}
