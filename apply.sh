#!/bin/bash

set -ex

terraform init -input=false
terraform plan -out=tfplan -input=false
terraform apply -input=false tfplan

sleep 15 ; echo "Fin du sleep!!"

ansible-playbook -i terraform.py playbook.yml

